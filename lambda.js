var config 			= require('config');
var authC 			= require('./authC').create(config);

exports.generateHash = function(event, context) {
    authC.generateHash(event)
        .then(function (authCData) {
            context.succeed(authCData);
        })
        .catch(function (err) {
            console.error(err);
            context.fail(err.msg);
        });
}

exports.echo = function(event, context) {
    var appInfo = require('./package.json');
    var info = {
        name: appInfo.name,
        version: appInfo.version,
        description: appInfo.description,
        author: appInfo.author,
        node: process.version
    };

   context.succeed(info);
}