var buffer 		= require('buffer');
var crypto 		= require('crypto');
var promise		= require('bluebird');

exports.create = function(config) {
	var defaultOpts = config.defaultOpts;
	return {
		generateHash : function (opts) {

			try {
				var url = opts.url || defaultOpts.url;
				var tokenName = opts.tokenName || defaultOpts.tokenName;
				var key = opts.key || defaultOpts.key;
				var expiryInSec = opts.expiryInSec || defaultOpts.expiryInSec;
				var delimiter = opts.delimiter || defaultOpts.delimiter;
				var algorithm = opts.algorithm || defaultOpts.algorithm;
				var acl = opts.acl || defaultOpts.acl;

				var now = new Date();
				var expiry = Math.round(now.setSeconds(now.getSeconds() + expiryInSec) / 1000);
				var params = 'exp=' + expiry + delimiter + 'acl=' + acl;
				
				var convertedKey = new Buffer(key, 'hex')
				var hash = crypto.createHmac(algorithm, convertedKey).update(params).digest('hex');
				
				params = params + delimiter + 'hmac=' + hash;
				params = encodeURIComponent(params);
				params = tokenName + '=' + params;

				var fullUrl = 	url + ((url.slice(-1) !== '?')? '?': '') + params;								

				return promise.resolve({
					hash: params,
					signedUrl: fullUrl 
				});
			}
			catch (err) {
				return promise.reject({msg: err});
			}
		}
	}
};