exports.create = function(cnfg) {

	var config = cnfg;
	var winston  = require('winston');
	require('winston-loggly');
	 
	winston.add(winston.transports.Loggly, {
		token: config.logging.token,
		subdomain: config.logging.subdomain,
		tags: config.logging.tags,
		json:true
	});

	return {
		log:  function (msg) {
			winston.log('info', msg);
		},
		error: function (msg) {
			winston.log('error', msg);
		}
	}
};