# Akamai Edge Authentication Service #

# Overview #
Generate Hash for Akamai Edge Authentication
Currently window and acl are supported only.

# Environments #
* [Poc](http://edgeauthc-poc.elasticbeanstalk.com/v1)

# Echo #

## Request ##
GET: /v1/echo

### Response ###
Status: 200

BODY:

```
{
  "name": "edge-auth-js",
  "version": "1.0.0",
  "description": "Generate Hmac hash for Akamai content authorization",
  "author": "Vlad Khazin <vladimir.khazin@icssolutions.ca>",
  "node": "v4.1.1"
}
```

## Request ##
POST: /v1

BODY (all fields are optional):
```
{
  "url": "http://securevod.zeefamily.tv/vod/ABCD.mp4/index.m3u8",
  "tokenName": "hdnts",
  "key": "d61ad40e319e90f83d62b0c0",
  "expiryInSec": 31536000,
  "acl": "/*",
  "delimiter": "~",
  "algorithm": "SHA256"
}
```

### Response ###
Status: 200

BODY:

```
{
  "hash": "hdnts=exp=1480400634~acl=/*~hmac=0b7fd231b747ae6d29c1f0bdf56ac21ed324dec46968d20a5861838c2bc1fcc1",
  "signedUrl": "http://securevod.zeefamily.tv/vod/ABCD.mp4/index.m3u8?hdnts=exp=1480400634~acl=/*~hmac=0b7fd231b747ae6d29c1f0bdf56ac21ed324dec46968d20a5861838c2bc1fcc1"
}
```

Concatenate source url with the hash separated by question mark eg:  
http://securevod.zeefamily.tv/vod/ABCD.mp4/index.m3u8?hdnts=exp=1480399110~acl=/*~hmac=37dea0846be987cfedf7d436018d9de170e02e24e7d59060045c6e2f840dfe3f