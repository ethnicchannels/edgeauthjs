/*********************************************************************************
Dependencies
**********************************************************************************/
var config 			= require('config');
var logger          = require('./logger').create(config);
var opbeat          = require('opbeat').start({
                          appId: config.monitoring.appId,
                          organizationId: config.monitoring.organizationId,
                          secretToken: config.monitoring.secretToken
                        });
var authC 			= require('./authC').create(config, logger);
var restify 	    = require('restify');
/*********************************************************************************/

/**********************************************************************************
Configuration
**********************************************************************************/
var appInfo 		= require('./package.json');
var port 			= process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000;
var ip              = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var server 			= restify.createServer();
/*********************************************************************************/

/**********************************************************************************
Constants
**********************************************************************************/
var routePrefix                     = '/v1';
/*********************************************************************************/

/**********************************************************************************
Setup
**********************************************************************************/
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());
server.opts(/.*/, function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", req.header("Access-Control-Request-Method"));
    res.header("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"));
    res.send(200);
    return next();
});
server.use(restify.gzipResponse());

/**********************************************************************************
End-points
**********************************************************************************/
//Echo
server.get({path: routePrefix + '/echo', flags: 'i'}, echo);
server.get({path: routePrefix, flags: 'i'}, echo);
server.get({path: '/', flags: 'i'}, echo);
server.get({path: '/echo', flags: 'i'}, echo);

function echo(req, res, next) {
    var info = {
        name: appInfo.name,
        version: appInfo.version,
        description: appInfo.description,
        author: appInfo.author,
        node: process.version
    };
    res.send(info);
    next();
}    

server.post({path: routePrefix + '/', flags: 'i'}, generateHash);

function generateHash(req, res, next) {
	var opts = parseRequest(req)
    authC.generateHash(opts)
        .then(function(authCData) {       
            res.send(authCData);
        })
        .catch(function(err) {           
            logger.error(err);
            res.send(400, err.msg);
        })
        .done(function(){
            next();
        });
};    

function parseRequest(req) {
    var output = {};
    if (typeof req.body == 'string') {
        output = JSON.parse(req.body);
    } else {
        output = req.body || {};
    }
    return output;
}
/*********************************************************************************/

/**********************************************************************************
Start the server and job
**********************************************************************************/
server.listen(port, ip, function() {
	var msg = 'Starting service using port \'{port}\' and environment \'{environment}\''
				.replace('{port}', port)
				.replace('{environment}', process.env.NODE_ENV)
	logger.log(msg);
});
/*********************************************************************************/